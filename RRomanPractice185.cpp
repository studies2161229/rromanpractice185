
#include <iostream>

class Player
{
public:
	Player()
	{
		name = "Yes";
		score = 1;
	}
	Player(std::string N, int S)
	{
		name = N;
		score = S;
	}
	std::string name;
	int score;

	void Show()
	{
		std::cout << "\nPlayer: " << name << "\nScore:  " << score << "\n";
	}
};



// "Spartans! This is where we fight! This is where they die!" -300
int main()
{
	system("Color 9");


	int size;
	std::cout << "How many players?\n";
	std::cin >> size;

	Player* Players = new Player[size];
	Players[0] = *new Player();

	//demand input of names and score
	for (int counter = 0; counter < size; counter++)
	{
		std::cout << "\nPlayer " << counter + 1 << " name:\n";
		std::cin >> Players[counter].name;
		std::cout << "\nPlayer " << counter + 1 << " score:\n";
		std::cin >> Players[counter].score;
	}

	//display players and score
	std::cout << "\n\nPlayers list: " << "\n";
	for (int counter = 0; counter < size; counter++)
	{
		Players[counter].Show();
	}

	std::cout << "\n\n\n";


	//This is supposed to be a sorting algorithm. Remains to be proven if it actually does anything
	int i, j;
	for (i = 0; i < size - 1; i++)
	{
		for (j = 0; j < size - i - 1; j++)
		{
			if (Players[j].score < Players[j + 1].score)
			{
				std::swap(Players[j].name, Players[j + 1].name);
				std::swap(Players[j].score, Players[j + 1].score);
			}
		}
	}

	//display players and score, hopefully sorted (impossible)

	std::cout << "\nPlayers sorted by score in descending order" << "\n";
	for (int counter = 0; counter < size; counter++)
	{ 
		std::cout << "\nPlayer: " << Players[counter].name << "\nScore:  " << Players[counter].score << "\n";
	}

	delete[] Players;
}
